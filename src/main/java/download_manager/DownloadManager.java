package download_manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;


/*
* Менеджер закачки. (синглтон)
* Его цель - посылать серверу команды, получать ответ, возвращать ответ или ошибку слушателю.
**/

public class DownloadManager {

    private static DownloadManager instance;
    private IDownloadManagerListener listener;

    private static final String SERVER_LINK = "http://api.fixer.io/"; // наш менеджер будет общаться только с api.fixer.io, так что зашиваю это тут

    private Thread sendCommandThread;

    private DownloadManager() {
    }

    public static DownloadManager getInstance() {
        if (instance == null)
            instance = new DownloadManager();
        return instance;
    }

    // установка слушателя
    public DownloadManager setListener(IDownloadManagerListener listener) {
        this.listener = listener;
        return this;
    }

    // отправка команды серверу
    public void sendCommand(String command) {
        if (this.listener != null) { // решил, что нет смысла что-то делать, если нас никто не слушает
            if (sendCommandThread == null) { // и мы не будем запускать более одного потока одновременно
                this.listener.onLoadStart();
                SendCommandTransport sendCommandTransport = new SendCommandTransport(command); // создаём транспорт
                sendCommandThread = new Thread(sendCommandTransport); // кидаем транспорт в поток
                sendCommandThread.start(); // запускаем поток
            }
            else
                listener.onLoadError("Download already in process. Please, wait."); // для корректности, сообщим слушателю, что менеджер закачки уже что-то делает
        }
    }

    // класс с интерфейсом runnable (я их называю "транспорт"), реализующий общение с сервером в отдельном потоке
    private class SendCommandTransport  implements Runnable {
        String command;

        public SendCommandTransport(String command) {
            this.command = command;
        }
        public void run() {
            String urlString = SERVER_LINK + command;
            String result = null;
            String errorMessage = null;
            //System.out.println("debug: SendCommandTransport: urlString = " + urlString);

            // для отладки, поставим задержку для этой операции в 3 секунды, иммитируя скачивание некоего большого объёма данных
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) { // ошибку для этой задержки не обрабатываю, т.к. это отладочный код, в релизе его не будет
            }

            try {
                URL url = new URL(urlString);

                // формируем и посылаем запрос
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET"); // мы общаемся только GET запросами, т.ч. не буду усложнять, просто пишу тут хардкодом GET
                int responseCode = urlConnection.getResponseCode();

                // ожидаем и обрабатываем ответ
                if (responseCode == 200) { // 200 это код статуса http запроса означающий, что всё хорошо
                    BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    String inputLine;
                    StringBuffer response = new StringBuffer();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    result = response.toString();
                }
                else
                    errorMessage = "Incorrect response code from server: " + responseCode; // коды для упрощения не расшифровываю, а просто показываю число (пока не нашёл, как их можно рассшифровать автоматом)
            }
            catch (MalformedURLException e) {
                errorMessage = "MalformedURLException: " + e.getMessage();
            }
            catch (UnsupportedEncodingException e) {
                errorMessage = "UnsupportedEncodingException: " + e.getMessage();
            }
            catch (FileNotFoundException e) {
                errorMessage = "FileNotFoundException: " + e.getMessage();
            }
            catch (Exception e) {
                errorMessage = "Exception: " + e.getMessage();
            }

            //System.out.println("debug: SendCommandTransport: result = " + result);
            //System.out.println("debug: SendCommandTransport: errorMessage = " + errorMessage);
            DownloadManager.getInstance().onLoadComplete(result, errorMessage);
        }
    }


    // действия по окончанию загрузки
    private synchronized void onLoadComplete(String result, String errorMessage) {
        sendCommandThread = null;

        if (listener != null) {
            if ((result != null)
                    && (errorMessage == null))       // идеальный результат, сразу говорим слушателю, что всё ок, и возвращаем ему данные
                listener.onLoadOk(result);
            else if (errorMessage != null)           // какая то ошибка (считаем, что ответ нам при ошибке не важен, и возвращаем слушателю только ошибку)
                listener.onLoadError(errorMessage);
            else if (result == null)                 // ошибка пуста, но и ответ сервера пуст - если вдруг сюда попадём, то возвращаем слушателю соответствующую ошибку
                listener.onLoadError("Response from server is empty");
        }
    }

}