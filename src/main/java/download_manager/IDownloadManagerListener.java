package download_manager;

public interface IDownloadManagerListener {
    // менеджер начал работу
    void onLoadStart();

    // менеджером успешно получен результат
    void onLoadOk(String result);

    // в процессе работы менеджера произошла критическая ошибка
    void onLoadError(String errorMessage);
}
