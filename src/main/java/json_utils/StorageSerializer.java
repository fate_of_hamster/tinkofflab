package json_utils;

import com.google.gson.*;
import storage.Storage;
import storage.StorageChildItem;
import storage.StorageItem;
import utils_date.UtilsDate;

import java.lang.reflect.Type;


/*
* Сериализация хранилища из JSON строки.
* Да, стоило, наверное, не изобретать велосипед, и воспользоваться стандартной сериализацией в массив байт. Сейчас посмотрел, там это делается прощу. Но уже не успею поменять до сдачи задания.
**/

public class StorageSerializer implements JsonSerializer<Storage> {

    public JsonElement serialize(Storage src, Type typeOfSrc, JsonSerializationContext context) {
        // форммируем массив элементов
        final JsonArray jsonItemsArray = new JsonArray();
        for (final StorageItem item : src.getItems()) {
            // элемент
            final JsonObject jsonItemObject = new JsonObject();
            jsonItemObject.addProperty("name", item.getName());

            // дети элемента
            final JsonArray jsonChildsArray = new JsonArray();
            for (final StorageChildItem child : item.getChildsList()) {
                final JsonObject jsonChildObject = new JsonObject();
                jsonChildObject.addProperty("name", child.getName());
                jsonChildObject.addProperty("rate", child.getRate());
                jsonChildObject.addProperty("date", UtilsDate.dateWithTimeToStringCET(child.getDate()));
                jsonChildsArray.add(jsonChildObject);
            }
            jsonItemObject.add("childs", jsonChildsArray);

            // добавляем элемент во бщий массив
            jsonItemsArray.add(jsonItemObject);
        }

        // добавляем формированный массив в итоговый объект
        final JsonObject jsonResultObject = new JsonObject();
        jsonResultObject.add("items", jsonItemsArray);

        return jsonResultObject;
    }
}
