package json_utils;

import com.google.gson.*;
import com.sun.istack.internal.Nullable;
import storage.Storage;
import storage.StorageChildItem;
import storage.StorageItem;
import utils_date.UtilsDate;

import java.lang.reflect.Type;
import java.util.*;

/*
* Десериализация хранилища из JSON строки.
* Да, стоило, наверное, не изобретать велосипед, и воспользоваться стандартной сериализацией в массив байт. Сейчас посмотрел, там это делается прощу. Но уже не успею поменять до сдачи задания.
**/

public class StorageDeserializer implements JsonDeserializer<Storage> {

    @Nullable
    public Storage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        Storage result = null;

        if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();

            // берём элементы хранилища
            JsonElement itemsJElement = jsonObject.get("items");
            if (itemsJElement.isJsonArray()) {
                JsonArray itemsJArray = itemsJElement.getAsJsonArray();

                // проходимся отдельно по каждому элементу
                List<StorageItem> items = new ArrayList<StorageItem>();
                for (int i = 0; i < itemsJArray.size(); i += 1) {

                    if (itemsJArray.get(i).isJsonObject()) {
                        JsonObject itemJObject = itemsJArray.get(i).getAsJsonObject(); // тут у нас теперь JSONObject элемента хранилища
                        String itemName = itemJObject.get("name").getAsString(); // тут у нас имя элемента

                        StorageItem item = new StorageItem(itemName); // (!) создали элемент (!)

                        // дойдём так же до его детей
                        JsonElement childsJElement = itemJObject.get("childs");
                        if (childsJElement.isJsonArray()) {
                            JsonArray childsJArray = childsJElement.getAsJsonArray();

                            // пройдёмся по каждому ребёнку
                            List<StorageChildItem> childs = new ArrayList<StorageChildItem>();
                            for (int j = 0; j < childsJArray.size(); j += 1) {
                                if (childsJArray.get(j).isJsonObject()) {
                                    JsonObject itemChildJObject = childsJArray.get(j).getAsJsonObject(); // тут у нас теперь JSONObject ребёнка элемента хранилища
                                    String childName = itemChildJObject.get("name").getAsString();
                                    double childRate = itemChildJObject.get("rate").getAsDouble();
                                    Date childDate = UtilsDate.stringToDateWithTimeCET(itemChildJObject.get("date").getAsString());
                                    StorageChildItem child = new StorageChildItem(childName, childRate, childDate);

                                    childs.add(child);
                                }
                            }
                            item.addChilds(childs);
                        }
                        items.add(item);
                    }
                }

                if (result == null)
                    result = new Storage();
                result.setItems(items);
            }
        }

        return result;
    }
}
