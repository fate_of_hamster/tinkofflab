package json_utils;

import com.google.gson.*;
import com.sun.istack.internal.Nullable;
import storage.StorageChildItem;
import storage.StorageItem;
import utils_date.UtilsDate;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Map;
import java.util.Set;


/*
* Десериализация ответа сервера, по поводу курса валют, из JSON строки.
**/
public class ApiResponceDeserializer implements JsonDeserializer<StorageItem> {

    @Nullable
    public StorageItem deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String name = null;
        Date date = null;
        String nameChild = null;
        double rate = -1;
        StorageItem result = null;

        // выбираем данные из JSON строки
        if (json.isJsonObject()) {
            JsonObject jsonObject = json.getAsJsonObject();

            name = jsonObject.get("base").getAsString();
            date = UtilsDate.stringToDateCET(jsonObject.get("date").getAsString());
            date = UtilsDate.addCurrentTimeCET(date); // добавляем ещё тут же текущее время! что бы знать до секунд когда именно мы последний раз запрашивали данные у сервера

            JsonElement ratesJsonElements = jsonObject.get("rates"); // для упрощения, не выношу десериализацию этого объекта в отдельный класс, т.к. она не большая
            if (ratesJsonElements.isJsonObject()) {
                Set<Map.Entry<String, JsonElement>> entries = ratesJsonElements.getAsJsonObject().entrySet();
                if (entries.size() > 0) {
                    Map.Entry<String, JsonElement> entry = entries.iterator().next();
                    nameChild = entry.getKey().toString();
                    rate = entry.getValue().getAsDouble();
                }
            }
        }

        // создаём итоговый объекта
        if ((name != null)
                && (date != null)
                && (nameChild != null)
                && (rate != -1)) { // только если все данные корректны
            StorageChildItem child = new StorageChildItem(nameChild, rate, date);
            result = new StorageItem(name, child);
        }

        return result;
    }
}
