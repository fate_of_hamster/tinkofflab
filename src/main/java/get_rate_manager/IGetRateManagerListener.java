package get_rate_manager;

public interface IGetRateManagerListener {

    // менеджер начал процесс получения данных
    void onRateGettedStart();

    // менеджером успешно получен результат
    void onRateGettedOk(String fromCurrency, String toCurrency, double rate);

    // в процессе работы менеджера произошла критическая ошибка
    void onRateGettedError(String errorMessage);
}
