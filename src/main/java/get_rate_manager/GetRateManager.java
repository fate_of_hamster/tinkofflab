package get_rate_manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import download_manager.DownloadManager;
import download_manager.IDownloadManagerListener;
import json_utils.ApiResponceDeserializer;
import storage.StorageChildItem;
import storage.StorageItem;
import storage.StorageManager;
import utils_date.UtilsDate;

import java.util.Date;

/*
* Менеджер для получения курса валют. (синглтон)
* Его цель - принять запрос на получение курса валют, найти его в хранилище (точнее спросить у менеджера хранилища) и вернуть,
* либо, если в храналище нет, то загрузить с сервера, поместить в хранилище (точнее попросить это сделать менеджера хранилища) и вернуть запросившему.
**/

public class GetRateManager implements IDownloadManagerListener {

    private static GetRateManager instance;
    private IGetRateManagerListener listener;

    public static GetRateManager getInstance() {
        if (instance == null)
            instance = new GetRateManager();

        return instance;
    }

    public GetRateManager setListener(IGetRateManagerListener listener) {
        this.listener = listener;
        return this;
    }

    // получение данных
    public void getRate(String fromCurrencyName, String toCurrencyName) {
        if (listener != null) {
            StorageChildItem storageChildItem = StorageManager.getInstance().getStorageChildItem(fromCurrencyName, toCurrencyName);

            if (isStorageChildItemActual(storageChildItem)) {
                // если нашли в хранилище и эти данные актуальны
                storageChildItem.updateDateLastGet(); // обновляем дату последнего обращения к элементу
                listener.onRateGettedOk(fromCurrencyName, toCurrencyName, storageChildItem.getRate()); // возвращаем результат слушателю
            }
            else {
                // иначе запрашиваем данные у сервера (я не стал в данном участке уходить от ТЗ - я получаю курсы валют поштучно, а не пакетно)
                DownloadManager
                        .getInstance()
                        .setListener(this) // менеджеру закачек даём ссылку на нас, как на его слушателя
                        .sendCommand("latest?base=" + fromCurrencyName + "&symbols=" + toCurrencyName);
            }
        }
    }


    // этот метод ответит на вопрос, являются ли сейчас актуальными найденные данные
    private boolean isStorageChildItemActual(StorageChildItem storageChildItem) {
        boolean result = false; // по умолчанию считаем, что данные не актуальны

        if (storageChildItem != null) { // если данные есть
            Date currentDateTime = UtilsDate.getCurrentDateCTE();
            Date dateTimeLastDownload = storageChildItem.getDate();

            Date currentDate = UtilsDate.getOnlyDateCET(dateTimeLastDownload);
            Date dateLastDownload = UtilsDate.getOnlyDateCET(currentDateTime);

            if ((dateLastDownload.before(currentDate))      // если последний раз скачивали вчера, то без вопросов сразу говорим, что нужно будет тянуть данные с сервера
                    || dateLastDownload.after(currentDate)) { // еси вдруг окажется, что скачивали "Завтра"... тоже обновляемся... хотя такого быть не должно, т.к. мы считаем, что время на устройстве точное
                result = true;
            }
            else if (currentDate.equals(dateLastDownload)) { // если даты равны
                // надо работать с временем
                Date currentTime = UtilsDate.getOnlyTimeCET(currentDateTime);
                Date timeLastDownload = UtilsDate.getOnlyTimeCET(dateTimeLastDownload);
                Date timeStartServerUpdate = UtilsDate.getTimeStartServerMayUpdateData();
                Date dateTimeLastGet = storageChildItem.getDateLastGet();

                if ((currentTime.before(timeStartServerUpdate))                               // текущее время РАНЬШЕ, чем время момента начала обновления данных на сервере
                        || (timeLastDownload.after(timeStartServerUpdate))                    // ИЛИ последнее скачивание данных было ПОСЛЕ момента начала обновления данных на сервере
                        || (UtilsDate.getDiffMinutes(dateTimeLastGet, currentDateTime) <= 2)) // ИЛИ с момента последнего обращения к этим данным прошло менее 2 минут
                    result = true;                                                            // todo: надо бы для чистоты вынести эту константу 2 в DownloadManager
            }
        }

        return result;
    }


    // началась загрузка
    public void onLoadStart() {
        if (listener != null)
            listener.onRateGettedStart();
    }

    // успешно получен результат
    public void onLoadOk(String result) {
        if (listener != null) {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(StorageItem.class, new ApiResponceDeserializer())
                    .create();
            StorageItem item = gson.fromJson(result, StorageItem.class);

            if ((item != null)
                    && (item.getChildsList() != null)
                    && (item.getChildsList().size() > 0)
                    && (item.getChildsList().get(0) != null)) { // мы берём первого ребёнка, т.к. в данном случае у объекта, полученного из JSON строки, всегда будет только один ребёнок
                StorageManager.getInstance().getStorage().addItem(item); // помещение объекта в хранилище
                listener.onRateGettedOk(
                        item.getName(),
                        item.getChildsList().get(0).getName(),
                        item.getChildsList().get(0).getRate()
                );
            }
            else
                listener.onRateGettedError("Incorrect answer from server"); // тоже упрощаю, не будем разбиратсья пока почему он некорректен
        }
    }


    // в процессе работы произошла критическая ошибка
    public void onLoadError(String errorMessage) {
        if (listener != null)
            listener.onRateGettedError(errorMessage);
    }
}
