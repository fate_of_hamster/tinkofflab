package utils_date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Просто сборник вспомогательных инструментов для работы с датами
 * Просто это ещё одна тема, в которой я слаб оказался. Так что создал этот файл, что бы набрать полезных инструментов
 */
public class UtilsDate {

    private static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";
    private static final String SIMPLE_TIME_FORMAT = "HH:mm:ss";
    private static final String SIMPLE_DATE_AND_TIME_FORMAT = SIMPLE_DATE_FORMAT + " " + SIMPLE_TIME_FORMAT;
    private static final String CTE_TIME_ZONE = "GMT+1"; // на сервере fixer.io время GMT+1 (CET – Central European Time)

    // todo: надо бы для чистоты вынести эту константу в DownloadManager
    private static final String SERVER_MAY_UPDATE_DATA_START = "15:50:00"; // время, начиная с которого мы следим за обновлением данных на сервере - т.е. начинаю следить с 15:50


    // конвертирование даты в строку
    public static String dateToStringCET(Date date) {
        return dateToStringEx(date, SIMPLE_DATE_FORMAT, CTE_TIME_ZONE);
    }
    public static String dateWithTimeToStringCET(Date date) {
        return dateToStringEx(date, SIMPLE_DATE_AND_TIME_FORMAT, CTE_TIME_ZONE);
    }

    public static String dateToStringEx(Date date, String formatString, String timeZoneName) {
        DateFormat format = getDateFormat(formatString, timeZoneName);
        String result = format.format(date);
        return result;
    }


    // конвертирование строки в дату
    public static Date stringToDateCET(String dateString) {
        return stringToDateEx(dateString, SIMPLE_DATE_FORMAT, CTE_TIME_ZONE);
    }
    public static Date stringToDateWithTimeCET(String dateString) {
        return stringToDateEx(dateString, SIMPLE_DATE_AND_TIME_FORMAT, CTE_TIME_ZONE);
    }

    public static Date stringToDateEx(String dateString, String formatString, String timeZoneName) {
        DateFormat format = getDateFormat(formatString, timeZoneName);
        try {
            return format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    // текущая дата и время
    public static Date getCurrentDateCTE() {
        // ПРИМЕЧАНИЕ: считаю, что время на устройстве верное (иначе ещё надо дописывать функционал сверки времени с каким либо сервером точного времени, если не ошибаюсь)
        return Calendar.getInstance(TimeZone.getTimeZone(CTE_TIME_ZONE)).getTime();
    }

    // взять только дату
    public static Date getOnlyDateCET(Date date) {
        return getDateAfterFormatCET(date, SIMPLE_DATE_FORMAT);
    }

    // взять только время
    public static Date getOnlyTimeCET(Date date) {
        return getDateAfterFormatCET(date, SIMPLE_TIME_FORMAT);
    }


    // переконвертировать время
    private static Date getDateAfterFormatCET(Date date, String formatName) {
        DateFormat format = getDateFormat(formatName, CTE_TIME_ZONE);
        try {
            return format.parse(format.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    // формат для времени
    private static DateFormat getDateFormat(String formatString, String timeZoneName) {
        DateFormat format = new SimpleDateFormat(formatString);
        if (timeZoneName != null)
            format.setTimeZone(TimeZone.getTimeZone(timeZoneName));
        return format;
    }


    // добавить текущее время к дате
    public static Date addCurrentTimeCET(Date date) {
        long sum = getOnlyDateCET(date).getTime() + getOnlyTimeCET(getCurrentDateCTE()).getTime();
        return new Date(sum);
    }


    // возвращает время, начиная с которого сервер может обновить данные
    public static Date getTimeStartServerMayUpdateData() {
        return stringToDateEx(SERVER_MAY_UPDATE_DATA_START, SIMPLE_TIME_FORMAT, CTE_TIME_ZONE);
    }


    // разница между двумя датами в минутах
    public static int getDiffMinutes(Date date1, Date date2) {
        return (int) ( (date2.getTime() - date1.getTime()) / (1000 * 60));
    }
}
