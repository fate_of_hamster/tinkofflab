package get_rate_user_interface;

import download_manager.DownloadManager;
import get_rate_manager.GetRateManager;
import get_rate_manager.IGetRateManagerListener;
import java.util.Scanner;

/*
* Этот класс является входной точкой в функционал запроса и отображения курсов валют.
* Его предназначение запросить у пользователя данные, прокинуть их в глубь программы(передать менеджеру закачек), дождаться ответа и отобразить его на экране.
**/
public class RateCurrency  implements IGetRateManagerListener {

    private SimpleProgressBar progressBar;
    public final static String SEPARATOR = "-------------------";

    public synchronized void execute() {
        getRateCurrency();
    }


    private void getRateCurrency() {
        // запрашиваем данные у пользователя
        Scanner in = new Scanner(System.in);

        System.out.println("Enter from currency:");
        String fromCurrencyName = in.nextLine().toUpperCase(); // todo: стоило бы ввести проверку на количество символов, не больше 3, но сейчас уже не успеваю

        System.out.println("Enter to currency:");
        String toCurrencyName = in.nextLine().toUpperCase();

        System.out.println(SEPARATOR);

        // передаём данные менеджеру
        // ПРИМЕЧАНИЕ: я не делаю проверку, поддерживается ли то, что ввели, сервером - исхожу из того, что лучше сервер пусть это решит
        GetRateManager
                .getInstance()
                .setListener(this)                           // устанавливаем себя, как слушателя для этого менеджера
                .getRate(fromCurrencyName, toCurrencyName);  // запускаем процесс получения курса нужных нам валют*/
    }

    // менеджер начал процесс получения данных
    public void onRateGettedStart() {
        progressBarStart();
    }

    // менеджером успешно получен результат
    public void onRateGettedOk(String fromCurrency, String toCurrency, double rate) {
        progressBarStop();
        System.out.println(fromCurrency + " => " + toCurrency + " : " + rate);
        doTryAgain();
    }

    // в процессе работы менеджера произошла критическая ошибка
    public void onRateGettedError(String errorMessage) {
        progressBarStop();
        System.out.println("Error: " + errorMessage);
        doTryAgain();
    }


    // процесс, позволяющий попробовать пользователю снова
    private void doTryAgain() {
        System.out.println(SEPARATOR);
        System.out.println("Try again? (Y/N)");
        if ((new Scanner(System.in)).nextLine().equals("Y")) {
            System.out.println(SEPARATOR);
            getRateCurrency();
        }
    }


    // запуск прогресс бара
    private void progressBarStart() {
        // создаём прогресс бар, если его ещё не было
        if (progressBar == null)
            progressBar = new SimpleProgressBar();

        // запускаем прогресс бар в параллельном потоке, если он ещё не был запущен
        if (!progressBar.isInProgress()) {
            Thread progressBarThread = new Thread(progressBar); // кидаем наш прогресс бар в отдельный поток
            progressBarThread.setDaemon(true);                  // при завершении главного поток, этот поток нам тоже не нужен будет более
            progressBarThread.start();                          // и запускаем его
        }
    }

    // остановка прогресс бара
    private void progressBarStop() {
        if ((progressBar != null)) {
            progressBar.stop();
        }
        System.out.write(13);// очищаем строку, на которой стоим
    }
}
