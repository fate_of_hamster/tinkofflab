package get_rate_user_interface;

/*
* Мини вспомогательный класс для прогресс бара.
* Прогресс бар для консоли я я делаю впервые, (я в обще, впервые работаю с java не для Андроида) так что, могут иметь мсто большие недоработки. Но у меня по плану стоит эту тему тщательно изучить.
**/
public class SimpleProgressBar implements Runnable {

    private static final int SLEEP_MILLIS = 400;  // задержка между шагами
    private static final int MAX_POINT_COUNT = 5; // максимальное кол-во символов в строке (по достижению, строка будет обнуляться)
    public static final String STEP_SIMBOL = ".";

    private boolean inProgress = false;
    private int countDisplayPoints;


    public boolean isInProgress() {
        return this.inProgress;
    }

    public void stop() {
        this.inProgress = false;
    }

    // мы имплементировались от Runnable, т.ч. этот метод будет вызываться в параллельном потоке
    public void run() {
        System.out.write(13); // очищаем строку, на которой стоим
        inProgress = true;
        countDisplayPoints = 0;

        // будем просто выводить символы STEP_SIMBOL на экран через System.out.print, с частотой SLEEP_MILLIS миллисекунд
        while (inProgress) {
            try {
                if (countDisplayPoints >= MAX_POINT_COUNT) { // очищение строки, при достижении максимального кол-ва символов
                    countDisplayPoints = 0;
                    System.out.write(13);
                }

                System.out.print(STEP_SIMBOL); // вывод символа
                countDisplayPoints += 1;

                Thread.sleep(SLEEP_MILLIS); // задержка
            } catch (InterruptedException e) {
            }
        }
    }
}
