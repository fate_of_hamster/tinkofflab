package storage;

import java.util.Date;


/*
* Менеджер для работы с хранилищем.
* Его цель - принять запрос на получение курса валют, найти его в хранилище и вернуть, либо, если в храналище нет, то загрузить с сервера, поместить в хранилище и вернуть запросившему.
**/

public class StorageManager {

    private static StorageManager instance;
    private Storage storage;

    public static StorageManager getInstance() {
        if (instance == null)
            instance = new StorageManager();
        return instance;
    }

    // конструктор
    private StorageManager() {
        storage = new Storage();
        storage.refillItemsFromCashFile(); // тут сразу заполняем хранилище на основе данных из кэш файла
    }

    // вернёт курс для указанных валют, если он есть в хранилище (плюс дату последнего обновления)
    public StorageChildItem getStorageChildItem(String fromCurrencyName, String toCurrencyName) {
        StorageChildItem result = null;
        StorageItem item = storage.getItemByName(fromCurrencyName);
        if (item != null)
            result = item.getChildByName(toCurrencyName);
        return result;
    }

    // вернёт хранилище
    public Storage getStorage() {
        return this.storage;
    }
}
