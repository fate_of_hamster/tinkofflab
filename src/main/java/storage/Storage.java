package storage;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import json_utils.StorageDeserializer;
import json_utils.StorageSerializer;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/*
* Хранилище
**/

public class Storage {

    private List<StorageItem> items; // содержимое хранилища (для простоты использую структуру List, т.к. данных немного)

    private static final String CASH_FILE_NAME = "RateCurrencyCash.txt"; // кэшировать данные буду просто в txt файл, просто сразу в папке с программой, просто в виде JSON объекта

    public Storage() {
        items = new ArrayList<StorageItem>();
    }

    // вернуть всё содержимое хранилища
    public void setItems(List<StorageItem> items) {
        this.items = items;
    }
    public List<StorageItem> getItems() {
        return this.items;
    }

    // взять элемент хранилища с указанным именем
    public StorageItem getItemByName(String name) {
        for (int i = 0; i < items.size(); i += 1)
            if (items.get(i).getName().equals(name))
                return items.get(i);
        return null;
    }

    // добавить элемент в хранилище
    public boolean addItem(StorageItem item) {
        boolean result;
        StorageItem existsItem = getItemByName(item.getName());
        if (existsItem == null)
            result = items.add(item);
        else
            result = existsItem.addChilds(item.getChildsList());

        // если в хранилище что-то изменилось, просто сразу записываем всё хранилище в файл (упрощаю, не занимаюсь мерджем, объём данных небольшой, времени тоже не много)
        if (result)
            saveItemsToCashFile();

        return result;
    }


    // кэширование: сериализация с записью в файл
    public void saveItemsToCashFile() {
        // сериализуем хранилище в данные для файла
        final Gson gson = (new GsonBuilder())
                .registerTypeAdapter(Storage.class, new StorageSerializer())
                .setPrettyPrinting()
                .create();
        String itemsGSONString = gson.toJson(this, Storage.class);

        // сохраняем данные в файл
        if (itemsGSONString != null) {
            FileWriter writeFile = null;
            try {
                File cashFile = new File(CASH_FILE_NAME);
                writeFile = new FileWriter(cashFile);
                writeFile.write(itemsGSONString);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (writeFile != null) {
                    try {
                        writeFile.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    // кэширование: чтение из файла с десериализацией
    public void refillItemsFromCashFile() {
        String itemsGSONString = null;

        // читаем данные из файла
        File cashFile = new File(CASH_FILE_NAME);
        if ((cashFile != null)
                && (cashFile.exists())) {
            StringBuilder sb = new StringBuilder();
            try {
                BufferedReader in = new BufferedReader(new FileReader(cashFile.getAbsoluteFile()));
                try {
                    String s;
                    while ((s = in.readLine()) != null) {
                        sb.append(s);
                        sb.append("\n");
                    }
                } finally {
                    in.close();
                }
            } catch(IOException e) {
                throw new RuntimeException(e);
            }
            itemsGSONString = sb.toString();
        }

        // десериализуем данные из файлы в хранилище
        if (itemsGSONString != null){
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Storage.class, new StorageDeserializer())
                    .create();
            Storage item = gson.fromJson(itemsGSONString, Storage.class);
            this.setItems(item.getItems());
        }
    }
}

