package storage;
import java.util.ArrayList;
import java.util.List;

/*
* Один элемент хранилища.
* По сути, это какая то валюта, и список валют, в которые она может конвертироваться.
**/

public class StorageItem {
    private String name;                               // наименование валюты
    private List<StorageChildItem> childsList;  // все валюты, в которые она конвертируется и сопутствующий этому курс (для простоты использую структуру List, т.к. данных немного)


    public StorageItem(String name) {
        this.name = name;
        this.childsList = new ArrayList<StorageChildItem>();
    }
    public StorageItem(String name, StorageChildItem child) {
        this.name = name;
        this.childsList = new ArrayList<StorageChildItem>();
        this.addChild(child);
    }
    public String getName() {
        return name;
    }
    public List<StorageChildItem> getChildsList() {
        return childsList;
    }


    // вернёт ребёнка с указанным именем
    public StorageChildItem getChildByName(String childName) {
        for (int i = 0; i < childsList.size(); i = i + 1)
            if (childsList.get(i).getName().equals(childName))
                return childsList.get(i);
        return null;
    }

    // пакетное добавление детей (вернёт true, если хотя бы один ребёнок был добавлен)
    public boolean addChilds(List<StorageChildItem> otherChildsList) {
        boolean result = false;
        for (int i = 0; i < otherChildsList.size(); i = i + 1)
            if (addChild(otherChildsList.get(i)))
                result = true;
        return result;
    }

    // добавить нового ребёнка
    private boolean addChild(StorageChildItem child) {
        boolean result = false;
        StorageChildItem existsChild = getChildByName(child.getName());
        if (existsChild != null) {
            if (existsChild.getRate() != child.getRate()) { // сверяем только курс валют - если он разный, значит заменяем элемент
                childsList.remove(existsChild);
                result = childsList.add(child);
            }
        }
        else
            result = childsList.add(child);
        return result;
    }
}
