package storage;

import utils_date.UtilsDate;

import javax.rmi.CORBA.Util;
import java.util.Date;

/*
* Один ребёнок одного элемента хранилища.
* По сути, это какая то валюта, в которую может конвертироваться её родитель, и соответствующий этому курс, и прочие вспомогательные данные.
**/

public class StorageChildItem {
    private String name;            // наименование валюты
    private double rate;            // курс, по которому произойдёт конвертация из валюты предка в эту валюту
    private Date date;              // дата последнего скачивания этой информации с сервера
    private Date dateLastGet;       // дата последнего обращения к этому элементу на клиенте

    public StorageChildItem(String name, double rate, Date date) {
        this.name = name;
        this.rate = rate;
        this.date = date;
        this.dateLastGet = UtilsDate.getCurrentDateCTE(); // при создании дату последнего обращения приравниваем текущей
    }

    public String getName() {
        return name;
    }
    public double getRate() {
        return rate;
    }
    public Date getDate() {
        return date;
    }
    public Date getDateLastGet() {
        return dateLastGet;
    }

    // обновляем дату обращения к этому элементу на клиенте
    public void updateDateLastGet() {
        this.dateLastGet = UtilsDate.getCurrentDateCTE();
    }

    public boolean equals(StorageChildItem otherObject) {
        return ((this.name.equals(otherObject.name))
                && (this.rate == otherObject.rate)
                && (this.date == otherObject.date));
    }
}
